class CountCorners
	require 'rubyXL'
	require 'set'
	require 'pry-debugger'

	file_name = "data_analysis.xlsx"
	begin_row = 1
	end_row = 6
	columns = [2,4,6]
	answer_count_column = 7
	answer_unique_column = 8

	workbook = RubyXL::Parser.parse(file_name)
	worksheet = workbook["Sheet1"]
	data = worksheet.extract_data

#binding.pry	
	for row in begin_row..end_row do
		corner_count = 0
		unique_corners = Set.new()
		for col in columns do
			failed_corners = data[row][col].split(',')
			for corner in failed_corners do
				unless ['success'].include?(corner)
					corner_count += 1
					unique_corners.add(corner)
				end
			end
		end
#binding.pry
		#data[row][answer_count_column].change_contents(corner_count)
		#data[row][answer_unique_column].change_contents(unique_corners.size)
		worksheet.insert_cell(row, answer_count_column, corner_count)
		worksheet.insert_cell(row, answer_unique_column, unique_corners.size)
binding.pry
	end

	
end
