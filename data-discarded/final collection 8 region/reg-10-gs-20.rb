class DrawImage
	require 'rubyXL'
	require 'RMagick'
	include Magick
	require 'set'
	require 'pry-debugger'
	
	number_of_regions = 10
number_of_regions = 3
number_of_regions = 5
number_of_regions = 8
	number_of_individuals = 149
number_of_individuals = 199
	file_name = "data7.xlsx"
	grid_size = 20
start_index = 73	#for 10 regions
start_index = 38	# for 3 regions
start_index = 48	# for 5 regions
start_index = 63	# for 8 regions
   	end_index = start_index + number_of_regions*4 -1 	#382 for 40 regions
	

	
	total_grids = (640*480)/(grid_size*grid_size)
	color_pallette = ["#FF0000", "#00FF00", "#0000FF", "#000000", "#FF00FF", "#00FFFF", "#FFFF00", 
        			  "#800000", "#008000", "#000080", "#808000", "#800080", "#008080", "#808080", 
        			  "#C00000", "#00C000", "#0000C0", "#C0C000", "#C000C0", "#00C0C0", "#C0C0C0", 
        			  "#400000", "#004000", "#000040", "#404000", "#400040", "#004040", "#404040", 
        			  "#200000", "#002000", "#000020", "#202000", "#200020", "#002020", "#202020", 
        			  "#600000", "#006000", "#000060", "#606000", "#600060", "#006060", "#606060", 
        			  "#A00000", "#00A000", "#0000A0", "#A0A000", "#A000A0", "#00A0A0", "#A0A0A0", 
        			  "#E00000", "#00E000", "#0000E0", "#E0E000", "#E000E0", "#00E0E0", "#E0E0E0"]
	workbook = RubyXL::Parser.parse(file_name)
	worksheet = workbook["Sheet1"]
	data = worksheet.extract_data

	list = ImageList.new
	canvas = Image.new(640,480) { 
		self.background_color = "white" 
		self.dispose = BackgroundDispose
	}
	grid_brush = Draw.new
	for col in 0..32
		grid_brush.line(col*grid_size,0, col*grid_size,480)
	end
	for row in 0..24
		grid_brush.line(0, row*grid_size, 640, row*grid_size)
	end

	grid_brush.draw(canvas)
	list << canvas

	(1..number_of_individuals).each_with_index do |individual_index, index|
		canvas = Image.new(640,480) { 
			self.background_color = "white"
		}
		grid_brush.draw(canvas)
		brush = Draw.new
		pallette_index = 0
		boxes = Set.new()
#binding.pry
		data[individual_index-1][start_index..end_index].each_slice(4) do |region|
			( [ region[3],region[1] ].min..[ region[3],region[1] ].max-1 ).each do |y|
				( [ region[2],region[0] ].min..[ region[2],region[0] ].max-1 ).each do |x|
					boxes.add(x+y*32)
				end
			end
			brush.fill( color_pallette[pallette_index] )
			brush.rectangle(region[0]*grid_size, region[1]*grid_size, region[2]*grid_size, region[3]*grid_size)
			pallette_index = pallette_index+1
		end
		brush.draw(canvas)
		brush.annotate(canvas, 0,0,0,40, 
				"#{index} - #{data[individual_index-1][0].split[7].to_f.round(5)} - #{boxes.size} - #{(boxes.size)*100/total_grids} %" 
				){
		    self.font_family = 'Helvetica'
		    self.pointsize = 20
		    self.font_weight = BoldWeight
		}
		list << canvas
		p index
	end
	list.animate(50)
	list.last.display
	avg_distribution = list.average
	avg_distribution.display
end
