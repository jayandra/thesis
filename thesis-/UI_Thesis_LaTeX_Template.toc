\contentsline {chapter}{Abstract}{iii}
\contentsline {chapter}{Acknowledgements}{iv}
\contentsline {chapter}{List of Tables}{vii}
\contentsline {chapter}{List of Figures}{viii}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {chapter}{\numberline {2}Background}{5}
\contentsline {section}{\numberline {2.1}Image Processing}{5}
\contentsline {section}{\numberline {2.2}Machine Learning}{6}
\contentsline {subsection}{\numberline {2.2.1}Unsupervised Learning}{7}
\contentsline {subsection}{\numberline {2.2.2}Reinforcement Learning}{7}
\contentsline {subsection}{\numberline {2.2.3}Supervised Learning}{8}
\contentsline {subsubsection}{\numberline {2.2.3.1}Learning From Demonstration}{8}
\contentsline {section}{\numberline {2.3}COTS Bots}{10}
\contentsline {section}{\numberline {2.4}Evolutionary Computation}{11}
\contentsline {section}{\numberline {2.5}GEFE and Evolving regions}{13}
\contentsline {section}{\numberline {2.6}Local Binary Pattern}{15}
\contentsline {section}{\numberline {2.7}Edge and Contour Detection}{17}
\contentsline {chapter}{\numberline {3}Goal}{18}
\contentsline {chapter}{\numberline {4}Experiment}{20}
\contentsline {section}{\numberline {4.1}Experimental Setup}{20}
\contentsline {section}{\numberline {4.2}Static 32 regions}{23}
\contentsline {subsection}{\numberline {4.2.1}Vision Training Step}{23}
\contentsline {subsection}{\numberline {4.2.2}Training and Evolution Step}{24}
\contentsline {subsection}{\numberline {4.2.3}Test}{28}
\contentsline {subsection}{\numberline {4.2.4}Result}{30}
\contentsline {section}{\numberline {4.3}Evolving regions using GEFE}{32}
\contentsline {subsection}{\numberline {4.3.1}Vision Training Step}{32}
\contentsline {subsection}{\numberline {4.3.2}Training and Evolution Step}{32}
\contentsline {subsection}{\numberline {4.3.3}Test for number of regions}{36}
\contentsline {subsection}{\numberline {4.3.4}Test for nature of regions}{42}
\contentsline {subsection}{\numberline {4.3.5}Test for learning by backpropagation}{44}
\contentsline {subsection}{\numberline {4.3.6}Results}{46}
\contentsline {chapter}{\numberline {5}Summary and Conclusions}{49}
\contentsline {chapter}{\numberline {6}Future Work}{51}
\contentsline {chapter}{References}{52}
