\contentsline {chapter}{Authorization to Submit Thesis}{ii}
\contentsline {chapter}{Abstract}{iii}
\contentsline {chapter}{Acknowledgements}{iv}
\contentsline {chapter}{Table of Contents}{v}
\contentsline {chapter}{List of Tables}{vii}
\contentsline {chapter}{List of Figures}{viii}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {chapter}{\numberline {2}Background}{4}
\contentsline {section}{\numberline {2.1}Image Processing}{4}
\contentsline {section}{\numberline {2.2}Genetic-based Feature Extraction}{5}
\contentsline {chapter}{\numberline {3}Experiment}{6}
\contentsline {section}{\numberline {3.1}Edge Detection}{6}
\contentsline {section}{\numberline {3.2}Texture Detection}{7}
\contentsline {section}{\numberline {3.3}Evolving Of Sub-Regions}{8}
\contentsline {section}{\numberline {3.4}Experimental Setup}{9}
\contentsline {chapter}{\numberline {4}Results}{12}
\contentsline {chapter}{\numberline {5}Summary and Conclusions}{13}
\contentsline {chapter}{Appendix A: Name}{14}
\contentsline {chapter}{Appendix B: Name}{14}
\contentsline {chapter}{References}{14}
