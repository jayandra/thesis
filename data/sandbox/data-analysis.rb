require 'pry-debugger'
require 'rubyXL'

class DataAnalysis
	def initialize
		@file_name = "./../data-collected.xlsx"
		@column1 = 2
		@column2 = 4
	end

	def format_corners(corners)
		return [corners] 	if corners.class != String
		return ["success"]	if corners == "success" 
		corners.split(",").map{|i| i.to_i}
	end

	def update_hash(corners_array, corners_hash)
		corners_array.each do |corner|
			if corners_hash[corner].nil?
				corners_hash[corner] = 1 	if corner.class == Fixnum
				corners_hash["success"] = 1	if corner== "success"
			else
				corners_hash[corner] = corners_hash[corner] + 1
			end
		end
	end

	def do_analysis
		workbook = RubyXL::Parser.parse(@file_name)
		worksheet = workbook["Sheet1"]
		data = worksheet.extract_data

		failed_corners_cw = Hash.new
		failed_corners_cc = Hash.new
		failed_corners_cw_count = 0
		failed_corners_cc_count = 0
		data.each_with_index do |content, index|
			p content[0] if index % 8 == 0

			if !content.nil? && index !=0				
				cw_corners = format_corners( content[@column1] )
				cc_corners = format_corners (content[@column2] )

				update_hash(cw_corners, failed_corners_cw)
				update_hash(cc_corners, failed_corners_cc)
			else
				p failed_corners_cw 
				p failed_corners_cc
				failed_corners_cw.clear
				failed_corners_cc.clear
			end

		end
	end
end

a = DataAnalysis.new
a.do_analysis


