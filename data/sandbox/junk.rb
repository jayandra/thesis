# top_corner = Array.new
# bot_corner = Array.new
# for y in 0..3
# 	for x in 0..7
# 		top_corner << x*4
# 		top_corner << y*6
# 		bot_corner << x*4+4
# 		bot_corner << y*6+6
# 	end
# end
# p top_corner
# p bot_corner

# i=0
# p top_corner.length
# while i < top_corner.length
# 	p "#{top_corner[i]}, #{top_corner[i+1]}   ->     #{bot_corner[i]}, #{bot_corner[i+1]}"
# 	i+=2
# end


# test fitness
# x1 = 28
# y1 = 18
# x2 = 32
# y2 = 24

# c=0
# num_rows = 24
# start_index = [y1, y2].min + [x1, x2].min*num_rows
# for i in 0..(x2-x1).abs-1
# 	for j in 0..(y2-y1).abs-1
# 		p start_index+i*num_rows+j
# 		c+=1
# 	end
# end
# p start_index
# p c


def function(a)
a.inject({}){ |a,b| a[b] = a[b].to_i + 1; a}.\
reject{ |a,b| b == 1 }.keys
end